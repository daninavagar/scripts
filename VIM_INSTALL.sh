#!/bin/bash

echo "This script is to configure vim correctly with plugins"

echo "Now we are going to clone the repostiorium of vim. To do this we first create the hidden vim folder: mkdir ~/.vim/bundle"

mkdir ~/.vim/bundle

echo "git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim"

git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

git clone https://gitlab.com/txemagon/nettenance.git ~/Documentos/txema

echo "now we move the corresponding folders"

cp ~/Documentos/txema/srv/nettenance/repository/classroom1-ubuntu-14.04/~/.vimrc ~/

cp -r ~/Documentos/txema/srv/nettenance/repository/classroom1-ubuntu-14.04/~/.vim/bundle ~/.vim

cp ~/Documentos/txema/srv/nettenance/repository/classroom1-ubuntu-14.04/~/.vim/plugins.vundle ~/.vim

cp ~/Documentos/txema/srv/nettenance/repository/classroom1-ubuntu-14.04/~/.vim/vundle.vim ~/.vim

vim +PluginInstall +qall



echo "WE FINISH"


